import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Tasks from './Tasks';

class App extends Component {
      render() {
        return (
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">ToDO лист</h1>
            </header>
            <p className="App-intro">
              Введите задачу в поле ниже!
            </p>
            <TaskField/>

          </div>
        );
      }
}

class TaskField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: ['Еда','Вода', 'Коктайлы'],
      newtask: ''
    };

  };
  add = (text) => {
      if(this.state.newtask.length>0){
        var arr = this.state.tasks;
        arr.push(text);
        this.setState({tasks: arr});
      }
      else{
        alert('Введите задачу!');
      }
  };
  handleChange = (event) => {
   this.setState({newtask: event.target.value});
 }
  taskDelete = (i) => {
    var arr = this.state.tasks;
    arr.splice(i, 1);
    this.setState({tasks: arr});
  };
  taskSave = (text, i) => {
    var arr = this.state.tasks;
    arr[i] = text;
    this.setState ({tasks: arr});
  };
  taskMaker = (item, i) => {
      return (
        <Tasks key={i} index={i} taskSave={this.taskSave} taskDelete={this.taskDelete}>
          {item}
        </Tasks>);
    };
  render() {
    return (
      <div>
      <div className="addField">
        <input type="text" className="ttl" defaultValue={this.state.newtask} placeholder="Название задачи" onChange={this.handleChange}></input>
        <textarea placeholder="Описание задачи" id="txtar"></textarea>
        <input type="button" onClick={this.add.bind(null, this.state.newtask)} value="Добавить"></input>
      </div>
        <div className="all">
          {
          this.state.tasks.map (this.taskMaker)
          }
        </div>
      </div>
)}
}

export default App;
